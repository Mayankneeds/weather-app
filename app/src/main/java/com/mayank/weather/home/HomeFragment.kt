package com.mayank.weather.home

import UserPref
import android.os.Bundle
import android.view.*
import android.widget.SearchView
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.asLiveData
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.mayank.weather.BaseApplication
import com.mayank.weather.R
import com.mayank.weather.citydetails.CityFragment
import com.mayank.weather.databinding.FragmentHomeBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * A fragment representing a list of Items.
 */
class HomeFragment : Fragment() {

    private lateinit var binding: FragmentHomeBinding
    private lateinit var adapter: CityRecyclerViewAdapter

    private lateinit var userPref: UserPref

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(true)

        binding.recyclerView.let {
            it.layoutManager = LinearLayoutManager(context)
            adapter = CityRecyclerViewAdapter({ city ->
                //on click
                gotoCityFragment(city)
            }, { city ->
                //delete city
                confirmDeleteDialog(city)
            })
            it.adapter = adapter
        }


        binding.fabAddLocation.setOnClickListener {
            binding.root.findNavController().navigate(R.id.action_home_fragment_to_mapsFragment)
        }

        (requireActivity().application as BaseApplication).userPref.userPreferencesFlow.asLiveData()
            .observe(viewLifecycleOwner) {
                userPref = it
                adapter.addCities(it.listOfCities.toList())

                /**
                 * If no location bookmarked.
                 */
                if (it.listOfCities.isEmpty()) {
                    binding.txtNoData.visibility = View.VISIBLE
                    binding.txtByMayank.visibility = View.VISIBLE
                } else {
                    binding.txtNoData.visibility = View.GONE
                    binding.txtByMayank.visibility = View.GONE
                }
            }

    }

    private fun gotoCityFragment(city: String) {
        val bundle = bundleOf(
            CityFragment.CITY to city,
            CityFragment.UNIT to userPref.getMetricType()
        )
        binding.root.findNavController().navigate(R.id.action_home_fragment_to_cityFragment, bundle)
    }

    private fun confirmDeleteDialog(city: String) = activity?.let {
        val builder = MaterialAlertDialogBuilder(it)
        builder.setTitle("Are you sure to delete?")
        builder.setPositiveButton("Delete") { dialog, i ->
            viewLifecycleOwner.lifecycleScope.launch(Dispatchers.IO) {
                (requireActivity().application as BaseApplication).userPref.removeCity(city)
                launch(Dispatchers.Main) { dialog.dismiss() }
            }
        }

        builder.create().show()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu, menu)

        menu.findItem(R.id.app_bar_help).setOnMenuItemClickListener {
            //goto help fragmen
            binding.root.findNavController().navigate(R.id.action_home_fragment_to_helpFragment)
            return@setOnMenuItemClickListener true
        }


    }
}