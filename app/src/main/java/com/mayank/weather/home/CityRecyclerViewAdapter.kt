package com.mayank.weather.home

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.mayank.weather.R
import com.mayank.weather.databinding.ItemCityBinding
import java.util.*


class CityRecyclerViewAdapter(
    val onClick: (city: String) -> Unit,
    val deleteCity: (city: String) -> Unit
) :
    RecyclerView.Adapter<CityRecyclerViewAdapter.ViewHolder>() {

    val listOFCities = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemCityBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val city = listOFCities[position]
        holder.binding.txtCity.text = city.capitalize(Locale.getDefault())
        holder.binding.root.setOnClickListener { onClick(city) }
        holder.binding.imgDelete.setOnClickListener { deleteCity(city) }
    }

    override fun getItemCount(): Int = listOFCities.size

    inner class ViewHolder(val binding: ItemCityBinding) : RecyclerView.ViewHolder(binding.root)

    fun addCities(listOFCities: List<String>) {
        this.listOFCities.clear()
        this.listOFCities.addAll(listOFCities)
        notifyDataSetChanged()
    }
}