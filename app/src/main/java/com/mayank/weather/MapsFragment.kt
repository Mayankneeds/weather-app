package com.mayank.weather

import androidx.fragment.app.Fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.mayank.weather.databinding.FragmentHomeBinding
import com.mayank.weather.databinding.FragmentMapsBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MapsFragment : Fragment() {

    private lateinit var binding: FragmentMapsBinding

    private val callback = OnMapReadyCallback { googleMap ->
        /**
         * Manipulates the map once available.
         * This callback is triggered when the map is ready to be used.
         * This is where we can add markers or lines, add listeners or move the camera.
         * In this case, we just add a marker near Sydney, Australia.
         * If Google Play services is not installed on the device, the user will be prompted to
         * install it inside the SupportMapFragment. This method will only be triggered once the
         * user has installed Google Play services and returned to the app.
         */
        val sydney = LatLng(-34.0, 151.0)
        googleMap.addMarker(MarkerOptions().position(sydney).title("Marker in Sydney"))
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney))

        googleMap.uiSettings.isZoomControlsEnabled = true

        googleMap.setOnMarkerDragListener(object : GoogleMap.OnMarkerDragListener {
            override fun onMarkerDragStart(marker: Marker) {

            }

            override fun onMarkerDrag(marker: Marker) {

            }

            override fun onMarkerDragEnd(marker: Marker) {
                /**
                 * val latLng = marker.position
                 * latLng.longitude + latLng.longitude
                 *
                 * Here I can save the LatLng or city name to the data store.
                 * But as we know billing needs to be enabled for Google Maps.
                 * I'm giving static city's list option for city selection.
                 */
            }

        })

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMapsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)

        binding.btnShowCities.setOnClickListener { openCitiesDialog() }

    }

    private fun openCitiesDialog() = activity?.let {
        val builder = MaterialAlertDialogBuilder(it)
        builder.setTitle(getString(R.string.choose_city))

        val cities = resources.getStringArray(R.array.cities)
        builder.setItems(cities) { dialog, which ->
            viewLifecycleOwner.lifecycleScope.launch(Dispatchers.IO) {

                (requireActivity().application as BaseApplication).userPref.addCity(cities[which])

                launch(Dispatchers.Main) {
                    dialog.dismiss()
                    Toast.makeText(context, "${cities[which]} Added", Toast.LENGTH_SHORT).show()
                    binding.root.findNavController().navigateUp()
                }
            }
        }

        builder.create().show()
    }
}