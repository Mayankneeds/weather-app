package com.mayank.weather.retrofit;


import java.io.Serializable;

import retrofit2.Call;

public class  BaseModel implements Serializable {

    private int requestCode;
    private Call call;

    public int getRequestCode() {
        return requestCode;
    }

    public void setRequestCode(int requestCode) {
        this.requestCode = requestCode;
    }

    public Call getCall() {
        return call;
    }

    public void setCall(Call call) {
        this.call = call;
    }
}
