package com.mayank.weather.retrofit;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * This class detects internet connection.
 */
public class ConnectionDetector {

    public static final String NO_INTERNET = "Check Your Internet Connection";


    public static boolean isConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean result = false;
        if (activeNetwork != null) {
            result = activeNetwork.isConnected();
        }
        return result;
    }

}
