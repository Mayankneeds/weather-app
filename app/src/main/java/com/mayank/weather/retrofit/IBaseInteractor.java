package com.mayank.weather.retrofit;

public interface IBaseInteractor {

    void execute(final BaseModel baseModel, final OnRetrofitListener onRetrofitListener);
}
