package com.mayank.weather.retrofit;


import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("data/2.5/weather?")
    Call<JsonObject> getTodayForCast(@Query("q") String query,
                                     @Query("units") String units,
                                     @Query("appid") String appId);

}