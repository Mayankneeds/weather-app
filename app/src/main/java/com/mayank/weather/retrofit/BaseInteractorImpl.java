package com.mayank.weather.retrofit;


import android.content.Context;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Admin
 */

public class BaseInteractorImpl implements IBaseInteractor {

    private final Context context;

    public BaseInteractorImpl(Context context) {
        this.context = context;
    }


    @Override
    public void execute(final BaseModel baseModel, final OnRetrofitListener onRetrofitListener) {
        if (ConnectionDetector.isConnected(context)) {
            baseModel.getCall().enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    onRetrofitListener.onServiceSuccess(response, baseModel.getRequestCode());

                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    onRetrofitListener.onServiceFailure(t.getLocalizedMessage(), baseModel.getRequestCode());
                }
            });
        } else {
            onRetrofitListener.onServiceFailure(ConnectionDetector.NO_INTERNET, 0);
        }
    }
}
