package com.mayank.weather.retrofit;


public interface OnRetrofitListener {

    void onServiceSuccess(Object o, int requestCode);

    void onServiceFailure(String t, int requestCode);
}
