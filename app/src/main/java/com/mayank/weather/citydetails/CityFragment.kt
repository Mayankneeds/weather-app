package com.mayank.weather.citydetails


import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.Spanned
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.google.android.material.snackbar.Snackbar
import com.google.gson.JsonObject
import com.mayank.weather.databinding.CityFragmentBinding
import com.mayank.weather.loadUrl
import com.mayank.weather.retrofit.NetworkResponse


class CityFragment : Fragment() {

    private lateinit var binding: CityFragmentBinding
    private val viewModel: CityViewModel by viewModels()
    private var snackBar: Snackbar? = null

    companion object {
        const val CITY = "city"
        const val UNIT = "unit"
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = CityFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.livedataResponse.observe(viewLifecycleOwner) {
            when (it) {
                is NetworkResponse.Success -> {
                    it.data?.let {
                        setUpUI(it)
                    }
                    //hide loading and show content
                    binding.swipeLayout.isRefreshing = false
                    binding.scrollContent.visibility = View.VISIBLE
                }
                is NetworkResponse.Error -> {
                    //hide laoding and show error with retry option
                    binding.swipeLayout.isRefreshing = false
                    snackBar = Snackbar.make(binding.root, it.message!!, Snackbar.LENGTH_INDEFINITE)
                        .setAction("Retry") {
                            getTodayForcast()
                        }
                    snackBar?.show()
                }

                is NetworkResponse.Loading -> {
                    binding.swipeLayout.isRefreshing = true
                }
            }
        }

        getTodayForcast()

        binding.swipeLayout.setOnRefreshListener {
            getTodayForcast()
            binding.swipeLayout.isRefreshing = false
        }
    }

    private fun getTodayForcast() = arguments?.let { bundle ->
        binding.txtCity.text = bundle.getString(CITY)!!
        viewModel.getTodayForcast(bundle.getString(CITY)!!, bundle.getString(UNIT)!!)
    }

    private fun setUpUI(jsonObject: JsonObject) {
        /**
         * Getting data from jsonObject which is needed.
         */
        val weatherInfo = jsonObject.get("weather").asJsonArray.get(0).asJsonObject
        val iconUrl =
            "https://openweathermap.org/img/wn/${weatherInfo.get("icon").asString}@2x.png"
        val weatherType = weatherInfo.get("main")?.asString
        val temp = jsonObject.get("main")?.asJsonObject?.get("temp")?.asString
        val humidity = jsonObject.get("main")?.asJsonObject?.get("humidity")?.asString
        val windSpeed = jsonObject.get("wind")?.asJsonObject?.get("speed")?.asString
        val windDegree = jsonObject.get("wind")?.asJsonObject?.get("deg")?.asString
        val windGust = jsonObject.get("wind")?.asJsonObject?.get("gust")?.asString
        val rainLast1Hr = jsonObject.get("rain")?.asJsonObject?.get("1h")?.asFloat
        val rainLast3Hr = jsonObject.get("rain")?.asJsonObject?.get("3h")?.asFloat

        /**
         * Shows Weather Image, Temperature, Weather Type, Humidity
         */
        binding.imgWeather.loadUrl(iconUrl)
        binding.txtWeatherType.text = "$weatherType - "
        binding.txtTemp.text = "$temp\u00B0"
        binding.txtHumidity.text = "Humidity: $humidity%"

        /**
         * Shows Wind Info.
         */
        binding.txtWindSpeed.text =
            getHtml("<h3>Speed</h3><br/>${windSpeed} meter/sec")

        binding.txtWindDegree.text =
            getHtml("<h3>Degree</h3><br/>$windDegree\u00B0")

        // Only Show gust, if it is presented.
        windGust?.let {
            binding.txtWindGust.visibility = View.VISIBLE
            binding.txtWindGust.text = getHtml("<h3>Gust</h3><br/>${it} meter/sec")
        }

        /**
         * Shows Rain Info.
         */
        binding.txtRainLast1hr.text =
            rainLast1Hr?.let { getHtml("<h3>Last 1Hr</h3><br/>$it mm") }
                ?: "No Data Available"

        // Only Show rain Last 3Hr, if it is presented.
        rainLast3Hr?.let {
            binding.txtRainLast3hr.visibility = View.VISIBLE
            binding.txtRainLast3hr.text = getHtml("<h3>Last 3Hr</h3><br/>$it mm")
        }
    }

    override fun onDetach() {
        super.onDetach()
        snackBar?.dismiss()
    }

    private fun getHtml(text: String): Spanned? =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT)
        else Html.fromHtml(text)

}