package com.mayank.weather.citydetails

import android.app.Application
import androidx.lifecycle.*
import com.google.gson.JsonObject
import com.mayank.weather.retrofit.*
import retrofit2.Response

class CityViewModel(application: Application) : AndroidViewModel(application) {

    private val APP_ID = "fae7190d7e6433ec3a45285ffcf55c86"

    private val mInteractor: IBaseInteractor
    private val mApiService: ApiInterface
    val livedataResponse = MutableLiveData<NetworkResponse<JsonObject>>()

    init {
        mInteractor = BaseInteractorImpl(application.applicationContext)
        mApiService = ApiClient.getClient().create(ApiInterface::class.java)
    }

    fun getTodayForcast(city: String, unit: String) {
        livedataResponse.value = NetworkResponse.Loading()

        val baseModel = BaseModel()
        baseModel.call = mApiService.getTodayForCast(city, unit, APP_ID)
        mInteractor.execute(baseModel, object : OnRetrofitListener {
            override fun onServiceSuccess(o: Any, requestCode: Int) {
                try {
                    val jsonObject = (o as Response<*>).body() as JsonObject

                    if (jsonObject.get("cod").asInt == 200)
                        livedataResponse.value = NetworkResponse.Success(jsonObject)
                    else
                        livedataResponse.value =
                            NetworkResponse.Error("Error Code: ${jsonObject.get("cod")}")

                } catch (e: Throwable) {
                    livedataResponse.value = NetworkResponse.Error(e.message)
                }
            }

            override fun onServiceFailure(error: String?, requestCode: Int) {
                val msg =
                    if (error == ConnectionDetector.NO_INTERNET) error else "Something went wrong"
                livedataResponse.value = NetworkResponse.Error("$msg")
            }
        })
    }
}


