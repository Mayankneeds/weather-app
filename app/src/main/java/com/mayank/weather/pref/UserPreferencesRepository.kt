

import android.content.Context
import android.util.Log
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.preferencesKey
import androidx.datastore.preferences.core.preferencesSetKey
import androidx.datastore.preferences.createDataStore
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map

class UserPreferencesRepository(context: Context) {

    private val TAG = "UserPreferencesRepository"

    private val dataStore: DataStore<Preferences> = context.applicationContext.createDataStore(
        name = USER_PREFERENCES_NAME,
        // migrations = listOf(SharedPreferencesMigration(context, USER_PREFERENCES_NAME))
    )

    val userPreferencesFlow: Flow<UserPref> = dataStore.data
        .catch { exception ->
            Log.e(TAG, exception.toString())
        }.map { preferences ->
            UserPref(
                preferences[IS_FIRST_RUN_EXECUTED] ?: false,
                preferences[LIST_OF_CITIES] ?: setOf(),
                preferences[IS_UNIT_MATRIC] ?: false,
            )
        }

    suspend fun setFirstRunExecuted(value: Boolean) = dataStore.edit { pref ->
        pref[IS_FIRST_RUN_EXECUTED] = value
    }

    suspend fun addCity(city: String) = dataStore.edit { pref ->
        val listOfCities = pref[LIST_OF_CITIES]?.toMutableSet() ?: mutableSetOf()
        listOfCities.add(city)
        pref[LIST_OF_CITIES] = listOfCities
    }

    suspend fun removeCity(city: String) = dataStore.edit { pref ->
        val listOfCities = pref[LIST_OF_CITIES]?.toMutableSet() ?: mutableSetOf()
        listOfCities.remove(city)
        pref[LIST_OF_CITIES] = listOfCities
    }

    companion object {
        private const val USER_PREFERENCES_NAME = "com.mayank.weather"
        private val IS_FIRST_RUN_EXECUTED = preferencesKey<Boolean>("is_first_run_executed")
        private val LIST_OF_CITIES = preferencesSetKey<String>("list_of_cities")
        private val IS_UNIT_MATRIC = preferencesKey<Boolean>("is_unit_matric")

    }
}