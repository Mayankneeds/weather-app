

data class UserPref(
    val isFirstRunExecuted: Boolean,
    val listOfCities: Set<String>,
    val isUnitMetric: Boolean,
) {
    fun getMetricType() = if (isUnitMetric) "metric" else "imperial"
}
