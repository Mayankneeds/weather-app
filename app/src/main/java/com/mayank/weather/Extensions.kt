package com.mayank.weather

import android.widget.ImageView
import com.bumptech.glide.Glide


fun ImageView.loadUrl(url: String) {
    Glide.with(this).load(url).placeholder(R.drawable.ic_refresh).into(this)
}