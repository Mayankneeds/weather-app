package com.mayank.weather

import UserPreferencesRepository
import androidx.multidex.MultiDexApplication


class BaseApplication : MultiDexApplication() {
    val userPref by lazy { UserPreferencesRepository(applicationContext) }
}